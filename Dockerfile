FROM ruby:2.5

RUN apt-get update && apt-get -y install nodejs

ADD . /app
WORKDIR /app
RUN bundle install

EXPOSE 3000
CMD ["rails", "s"]